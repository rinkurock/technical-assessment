# Technical Assessment

You are building a backend service and a database for a food delivery platform, with the following 2 raw datasets:

1. **Restaurants data**

File: restaurant_with_menu.json

This dataset contains a list of restaurants with their menus and prices, as well as their cash balances. This cash balance is the amount of money the restaurants hold in their merchant accounts on this platform. It increases by the respective dish price whenever a user purchases a dish from them.

2. **Users data**

File: users_with_purchase_history.json

This dataset contains a list of users with their transaction history and cash balances. This cash balance is the amount of money the users hold in their wallets on this platform. It decreases by the dish price whenever they purchase a dish.

These are all raw data, which means that you are allowed to process and transform the data, before you load it into your database.

# The Task

The task is to build an **API server, with documentation and a backing relational database (MySQL / PostgreSQL)** that will allow a front-end client to navigate through that sea of data easily, and intuitively. The front-end team will later use that documentation to build the front-end clients. **We'd much prefer you to use Go / Python as a bonus.**

The operations the front-end team would need you to support are:

1. List all restaurants that are open at a certain datetime
2. List top y restaurants that have **more or less** than x number of dishes within a price range, ranked alphabetically. **More or less (than x)** is a parameter that the API allows the consumer to enter.
3. Search for restaurants or dishes by name, ranked by relevance to search term
4. Process a user purchasing a dish from a restaurant, handling all relevant data changes in an atomic transaction. Do watch out for potential race conditions that can arise from concurrent transactions!

In your repository, you would need to **document the API interface**, the **commands to run the ETL** (extract, transform and load) script that takes in the raw data sets as input, and outputs to your database, and the **command to set up your server and database**. It would be best to use **docker** to ensure a uniform setup across environments.

# Bonus
*This is optional, and serves as additional proof points. We will consider it complete even without this functionality*

Write appropriate tests with an appropriate coverage report.